import "./PostSiswa.css";
import { useState } from "react";

function PostSiswa() {
  const [nisn, setNisn] = useState("");
  const [nama, setNama] = useState("");
  const [hobi, setHobi] = useState("");
  const [message, setMessage] = useState("");

  let handleSubmit = async (e) => {
    e.preventDefault();
    try {
      var requestOptions = {
        method: 'POST',
        headers:{ 'Content-Type': 'application/json'},
        body: JSON.stringify({
          nisn: nisn,
          nama: nama,
          hobi: hobi,
        })
      };

      let res = await fetch("http://localhost:10000/create_data_siswa", requestOptions)

      if (res.status === 200) {
        setNisn("");
        setNama("");
        setMessage("Berhasil Memasukkan Data Siswa");
      } else {
        setMessage("Berhasil Memasukkan Data Siswa");
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="PostSiswa">
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={nisn}
          placeholder="NISN"
          onChange={(e) => setNisn(e.target.value)}
        />
        <input
          type="text"
          value={nama}
          placeholder="Nama"
          onChange={(e) => setNama(e.target.value)}
        />
        <input
          type="text"
          value={hobi}
          placeholder="Hobi"
          onChange={(e) => setHobi(e.target.value)}
        />

        <button type="submit">Kirim</button>

        <div className="message">{message ? <p>{message}</p> : null}</div>
      </form>
    </div>
  );
}

export default PostSiswa;