import React from "react";
import "./GetSiswa.css";

class GetSiswa extends React.Component {
    // Constructor
    constructor(props) {
        super(props);
 
        this.state = {
            items: [],
            DataisLoaded: false,
        };
    }
 
    componentDidMount() {
        fetch("http://localhost:10000/read_data_siswa")
            .then((res) => res.json())
            .then((json) => {
                this.setState({
                    items: json.data,
                    DataisLoaded: true,
                });
            });
    }
    render() {
        const { DataisLoaded, items } = this.state;
        if (!DataisLoaded)
            return (
                <div>
                    <h1> Harap Tunggu! Proses Pengambilan Data... </h1>
                </div>
            );
 
        return (
            <div className="GetSiswa">
                <h1>Data Siswa</h1>
                    <div className="container">
                        <table>
                          <th>ID</th>
                          <th>NISN</th>
                          <th>Nama</th>
                          <th>Hobi</th>
                        
                          {items.map((item) => {
                              return (
                                <tr key={item.id_siswa}>
                                    <td>{item.id_siswa}</td>
                                    <td>{item.nisn}</td>
                                    <td>{item.nama}</td>
                                    <td>{item.hobi}</td>
                                </tr>
                              )
                          })}

                        </table>
                    </div>
            </div>
        );
    }
}
 
export default GetSiswa;