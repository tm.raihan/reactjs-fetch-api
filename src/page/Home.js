function Home() {
  return (
    <div className="App">
      <h1>Halaman Utama WEB</h1>
      <p>Silahkan gunakan menu <b>GET</b> untuk melihat data siswa</p>
      <p>Silahkan gunakan menu <b>POST</b> untuk memasukkan data siswa</p>
    </div>
  );
}

export default Home;
