import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Home from "./page/Home"
import PostSiswa from "./page/PostSiswa"
import GetSiswa from "./page/GetSiswa"

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <a href="/" className="Navbar">HOME</a>
        <a href="/GetSiswa" className="Navbar">GET</a>
        <a href="/PostSiswa" className="Navbar">POST</a>
      </header>

      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/GetSiswa" element={<GetSiswa/>} />
          <Route path="/PostSiswa" element={<PostSiswa/>} /> 
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
